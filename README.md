
# Rishabh Romit's Connectiviy Demo !

Hi! This is my submission for the *SnapTravel Connectivity Demo* challenge.

I've forked the repo from [https://gitlab.com/snaptravel/connectivity-demo](https://gitlab.com/snaptravel/connectivity-demo) and added the commit titled 'start' as per the requirements.

The following is a short git summary of commits made on top of this 'start' commit:
```
    * 04a747e - (HEAD -> master, origin/master, origin/HEAD) Scripts: Add comments and helper notes to improve code readability throughout the scripts (CLI) (14 minutes ago) <Rishabh Romit>
    * d7efc48 - API: Add comments and helper notes to improve code readability throughout the SnapTravel Connectivity Demo Flask Application (70 minutes ago) <Rishabh Romit>
    * d493b2f - Init: Add comments and helper notes to improve code readability (6 hours ago) <Rishabh Romit>
    * 2b49f99 - Alembic: Add migration to include city name in hotel schema (6 hours ago) <Rishabh Romit>
    * 51be4fc - Setup.sh: Add print and script help statements (6 hours ago) <Rishabh Romit>
    * 9bda041 - API, Models: Add support to store city name in database (hotel schema) (7 hours ago) <Rishabh Romit>
    * 530f39b - API: Initial Commit with Models, API, Services, helper utils & scripts (7 hours ago) <Rishabh Romit>
    * d157a3c - Alembic: Add support for using alembic as the database migration tool (7 hours ago) <Rishabh Romit>
    * 2003e07 - Env, Requirements: Add intial dev environment and requirements files (7 hours ago) <Rishabh Romit>
    * 82a8e6a - setup.sh: Add a simple setup script to install dependencies required to kick start the connectivity demo (7 hours ago) <Rishabh Romit>
    * a7d1bdf - .git: Add .gitignore file (7 hours ago) <Rishabh Romit>
    * feb7433 - start (34 hours ago) <Rishabh Romit>
```


# Folder & File Structure

> These **links** **take** **you** **directly** to the **folder**/**files** or the **exact line of code**

 1. [DOCS](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/DOCS) - A collection of documents which includes but is not limited to screenshots of the sample CLI usage.<br/>
 2. [alembic](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/alembic) - Have used alembic as a DB migration tool. This folder contains the autogenerated env files, autogenerated but [modified](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/alembic/env.py#L96) for this Demo files and [migration versions](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/alembic/versions) applied for this demo.<br/>
 3. [api](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/api) - This is the heart of the Flask API written for this Demo.<br/>
	 a) [core](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/api/core)  - defines the components used through out the Flask Application - [database](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/core/database/__init__.py) & a [services registry](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/core/services.py) component.<br/>
	 b) [hotels](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/api/hotels) - The hotels module which defines
    the SQLAlchemy type models ([Hotel](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/hotels/models.py#L13) & [HotelPrice](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/hotels/models.py#L75) Model) for data base persistance (PostgreSQL),
    [services](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/hotels/services.py),
    [utilities](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/hotels/utils.py) and
    [constants](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/hotels/constants.py) used for creating, updating and fetching the said
    [Hotel](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/hotels/services.py#L19) & 
    [HotelPrice](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/hotels/services.py#L62) objects.<br/>
	 c) [scripts](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/api/scripts) - A framework which allows the SnapTravel Connectitiy Demo Flask Application to have a CLI style interface. See Usage below.<br/>
		 c.1) [hotel_conectivity.py](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/scripts/hotels/hotel_conectivity.py) - This is where the connectivity demo endpoints are hit (*multi-threaded*), their responses normalized and their content persisted in the PostgreSQL database using the flask applications re-usable modules and services.<br/>
	 d) [utils](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/api/utils) - A number of helper utilities used by the services defined under hotels
module. This promotes readablility, maintainability & re-usablility.<br/>
	 e) [__init__.py](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/__init__.py) - This entry point which creates the SnapTravel Connectivity Demo flask
Application.<br/>
	 f) [create_flask_application.py](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/create_flask_application.py) - The modules where the actual creation of the SnapTravel Connectivity Demo flask Application.<br/>
NOTE: This is called regardless of whether the application is used as a CLI or
via uwsgi.<br/>
	 g) [settings.py](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/settings.py) - A collection of global settings used throughout the SnapTravel Connectivity Demo Flask Application.<br/>
	 h) [uwsgi.py](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/uwsgi.py) - A module which allows the SnapTravel Connectivity Demo Flask Application to be as a local development server.<br/>
	 i) [version.py](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/version.py) - This file defines where we store the version of SnapTravel Connectivity Demo Flask Application.<br/>
 4. [scripts](https://gitlab.com/rishabh.romit/connectivity-demo/tree/master/scripts) - This contains simple scripts to kick start the SnapTravel Connectivity Demo CLI.<br/>
 5. [.env](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/.env) - We use .env files to store local development environment variables in the local directory - these are loaded into the local environment of the flask application.<br/>
 6. [Procfile](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/Procfile) - We use honcho and Procfile style setup to launch a local deveopment server (uwsgi), if needed.<br/>
 7. [README.md](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/README.md) - This README file.<br/>
 8. [alembic.ini](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/alembic.ini) - Autogenerated alembic init file.<br/>
 9. [setup.sh](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/setup.sh) - This helper BASH script drives the setup & initialization of all packages (pip, virtualenv etc...), dependencies and kick starts the PostgreSQL (Docker container).<br/>
 **NOTE:** Having docker application installed and running is a pre-requisite.
 See: [https://docs.docker.com/v17.09/engine/installation/](https://docs.docker.com/v17.09/engine/installation/) if required.
 11. [requirements.txt](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/requirements.txt) - Used by pip (in setup.sh above) to install all packages and their dependencies used within the Flask Application.
 12. [uwsgi.ini](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/uwsgi.ini) - Uwsgi config file - looks for [api.uwsgi](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/api/uwsgi.py) module to kick start things when running as a local development server.

# Usage
 1. Run the `./setup.sh` bash script in a terminal (may take a few seconds depending on internet speed).
 2. At the end of the script, if successful, it will dump the sample usage of the connectivity demo CLI like this:
```
[INFO] 2020-01-08 01:09:30,531 - api.create_flask_application - Init :: Creating Flask Application
[INFO] 2020-01-08 01:09:30,533 - api.create_flask_application - Init :: Loading Application Settings
[INFO] 2020-01-08 01:09:30,534 - api.create_flask_application - Init :: Loading Core Extensions
[INFO] 2020-01-08 01:09:30,534 - api.create_flask_application - Init :: Initializing Service Registry
[INFO] 2020-01-08 01:09:30,534 - api.create_flask_application - Init :: Registering Modules...
[INFO] 2020-01-08 01:09:30,562 - api.hotels - RegiterModule :: hotels
[INFO] 2020-01-08 01:09:30,562 - api.hotels - RegiterService :: hotel_update_service
[INFO] 2020-01-08 01:09:30,563 - api.hotels - RegiterService :: hotel_price_update_service
[INFO] 2020-01-08 01:09:30,563 - api.hotels - RegiterService :: hotel_service
[INFO] 2020-01-08 01:09:30,563 - api.create_flask_application - Init :: Welcome to SnapTravel Connectivity DEMO! [app_version: 1.0.0]
[INFO] 2020-01-08 01:09:30,563 - api - Running the script manager...

usage: hotels hotel_connectivity_demo [-?] --city CITY --checkin CHECK_IN_DATE
                                      --checkout CHECK_OUT_DATE
                                      [--overwrite_prices OVERWRITE_PRICES]

A class which handles the SnapTravel Connectivity Demo being called via a CLI.
NOTE: We an optional argument style options to pass in params to the while
running this command.

optional arguments:
  -?, --help            show this help message and exit
  --city CITY, -c CITY  Checks hotel prices for a particular city. Usage:
                        -c=<city_name> [Required: True]
  --checkin CHECK_IN_DATE, -ci CHECK_IN_DATE
                        Checks hotel prices for a particular check out date.
                        Usage: -ci=<iso_8601_date> [Required: True]
  --checkout CHECK_OUT_DATE, -co CHECK_OUT_DATE
                        Checks hotel prices for a particular check out date.
                        Usage: -co=<iso_8601_date> [Required: True]
  --overwrite_prices OVERWRITE_PRICES, -o OVERWRITE_PRICES
                        Overwrites Prices if existing prices found for
                        check_in_date, check_out_date & same hotel_id Usage:
                        -o1 [Required: False]
```
## Sample usage with output:
`(connectivity-demo) <prompt> ./scripts/run python manage.py hotels hotel_connectivity_demo -c=toronto -ci=2020-07-01 -co=2020-07-05`

```
[INFO] 2020-01-08 01:15:39,904 - api.create_flask_application - Init :: Creating Flask Application
[INFO] 2020-01-08 01:15:39,906 - api.create_flask_application - Init :: Loading Application Settings
[INFO] 2020-01-08 01:15:39,906 - api.create_flask_application - Init :: Loading Core Extensions
[INFO] 2020-01-08 01:15:39,906 - api.create_flask_application - Init :: Initializing Service Registry
[INFO] 2020-01-08 01:15:39,906 - api.create_flask_application - Init :: Registering Modules...
[INFO] 2020-01-08 01:15:39,924 - api.hotels - RegiterModule :: hotels
[INFO] 2020-01-08 01:15:39,924 - api.hotels - RegiterService :: hotel_update_service
[INFO] 2020-01-08 01:15:39,924 - api.hotels - RegiterService :: hotel_price_update_service
[INFO] 2020-01-08 01:15:39,924 - api.hotels - RegiterService :: hotel_service
[INFO] 2020-01-08 01:15:39,924 - api.create_flask_application - Init :: Welcome to SnapTravel Connectivity DEMO! [app_version: 1.0.0]
[INFO] 2020-01-08 01:15:39,924 - api - Running the script manager...

[INFO] 2020-01-08 01:15:39,931 - api.scripts.hotels.hotel_conectivity - HotelConnectivityCommand :: Fetching Prices for toronto from: 2020-07-01 to: 2020-07-05
[INFO] 2020-01-08 01:15:40,081 - api.scripts.hotels.hotel_conectivity - HotelConnectivityCommand :: Normalizing hotel & price information

[INFO] 2020-01-08 01:15:40,149 - api.scripts.hotels.hotel_conectivity - HotelConnectivityCommand :: Hotel and Price Information created/updated as -
Hotel Name: The Seven Seasons Hotel,
Hotel ID: 87,
City: Toronto,
Check-In: 2020-07-01,
Check-out: 2020-07-05,
SnapTravelPrice: 101.0000,
RetailPrice: 101.0000

[INFO] 2020-01-08 01:15:40,169 - api.scripts.hotels.hotel_conectivity - HotelConnectivityCommand :: Hotel and Price Information created/updated as -
Hotel Name: The Pennsylvanian Factory Hotel,
Hotel ID: 66,
City: Toronto,
Check-In: 2020-07-01,
Check-out: 2020-07-05,
SnapTravelPrice: 166.9900,
RetailPrice: 190.9900

[INFO] 2020-01-08 01:15:40,190 - api.scripts.hotels.hotel_conectivity - HotelConnectivityCommand :: Hotel and Price Information created/updated as -
Hotel Name: The Main Continental Hotel,
Hotel ID: 51,
City: Toronto,
Check-In: 2020-07-01,
Check-out: 2020-07-05,
SnapTravelPrice: 101.9900,
RetailPrice: 110.9900

[INFO] 2020-01-08 01:15:40,212 - api.scripts.hotels.hotel_conectivity - HotelConnectivityCommand :: Hotel and Price Information created/updated as -
Hotel Name: Famous Lucky Dragon Hotel,
Hotel ID: 81,
City: Toronto,
Check-In: 2020-07-01,
Check-out: 2020-07-05,
SnapTravelPrice: 128.9900,
RetailPrice: 166.9900
```

# Troubleshooting
 1. If you see the message `This setup assumed Docker is installed on this machine.
  If not, see: https://docs.docker.com/v17.09/engine/installation/` this means docker application is not installed or running locally.
 2. The requirements.txt do not list `psycopg2` since macOS has problems installing it directly via pip. To overcome this I've added a line in the [setup.sh](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/setup.sh) to install it directly using LDFLAGS option -`env LDFLAGS="pg_config --ldflags" pip install psycopg2==2.8.3 --no-binary psycopg2`.<br/>
 If you're not on macOS or have issues - remove/comment this [line](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/setup.sh#L36) from [setup.sh](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/setup.sh) and add `psycopg2==2.8.3` directly to [requirements.txt](https://gitlab.com/rishabh.romit/connectivity-demo/blob/master/requirements.txt)

# Contact
Maintainer Name: Rishabh Romit<br/>
Maintainer Email: rishabh.romit@gmail.com
