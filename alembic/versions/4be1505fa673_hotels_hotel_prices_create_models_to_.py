"""Hotels, Hotel Prices: Create models to store hotel and price information

Revision ID: 4be1505fa673
Revises: 5df14941be13
Create Date: 2020-01-06 18:42:44.290926

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '4be1505fa673'
down_revision = '5df14941be13'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('hotels',
        sa.Column('id', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('hotel_id', sa.Integer(), nullable=False),
        sa.Column('hotel_name', sa.String(length=100), nullable=False),
        sa.Column('address', sa.Text(), nullable=False),
        sa.Column('num_reviews', sa.Integer(), nullable=True),
        sa.Column('num_stars', sa.Integer(), nullable=True),
        sa.Column('image_url', sa.Text(), nullable=True),
        sa.Column('amenities', postgresql.ARRAY(sa.String()), nullable=True),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_hotels'))
    )

    op.create_index('uq_idx_hotel_id', 'hotels', ['hotel_id'], unique=True)

    op.create_table('hotel_prices',
        sa.Column('id', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True),
        sa.Column('hotel_id', sa.Integer(), nullable=False),
        sa.Column('check_in_date', sa.Date(), nullable=False),
        sa.Column('check_out_date', sa.Date(), nullable=False),
        sa.Column('snap_travel_price', sa.Numeric(precision=10, scale=4),
                  nullable=False),
        sa.Column('retail_price', sa.Numeric(precision=10, scale=4),
                  nullable=False),
        sa.ForeignKeyConstraint(['hotel_id'], ['hotels.hotel_id'],
                                name='fk_hotel_prices__hotel_id__hotels__hotel_id'),
        sa.PrimaryKeyConstraint('id',
                                name=op.f('pk_hotel_prices'))
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('hotel_prices')
    op.drop_index('uq_idx_hotel_id', table_name='hotels')
    op.drop_table('hotels')
    # ### end Alembic commands ###
