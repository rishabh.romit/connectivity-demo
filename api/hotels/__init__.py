# -*- coding: utf-8 -*-

"""The heart of our SnapTravel Connectivity Demo - the hotels module.

The init deals with module & models initialization & services registery.
"""

import logging

from flask import Blueprint

from .models import (
    Hotel,
    HotelPrice,
)

from .services import (
    HotelUpdateService,
    HotelPriceUpdateService,
    HotelService,
)

__all__ = [
    'Hotel',
    'HotelPrice'
]

logger = logging.getLogger(__name__)

def init(app):
    blueprint = Blueprint('hotels', __name__)

    app.register_blueprint(blueprint)
    logger.info("RegiterModule :: hotels")

    #
    # Registering with the service registry makes these services available
    # throughout the application. As mentioned in the ServiceRegistry notes
    # this promotes a modular micro-service based approach to building
    # applications.
    #
    app.services.register_service('hotel_update_service',
                                  services.HotelUpdateService())
    logger.info("RegiterService :: hotel_update_service")

    app.services.register_service('hotel_price_update_service',
                                  services.HotelPriceUpdateService())
    logger.info("RegiterService :: hotel_price_update_service")

    app.services.register_service('hotel_service',
                                  services.HotelService())
    logger.info("RegiterService :: hotel_service")
