# -*- coding: utf-8 -*-

"""A collection of models to be used throughout the hotels module.

This is also used by other modules, services and scripts within the SnapTravel
Connectivity Demo Flask Application.
"""

import sqlalchemy as sa
from api.core.database import Model, db
from sqlalchemy.dialects import postgresql as pg

class Hotel(Model):
    """The hotel models stores all the meta-information regarding a hotel.

    NOTE: We use a field ID to depict the internal ID (primary key)
    within our application and store the as 'hotel_id' the 'id'
    from the connectivity demo endpoints.

    The table defines a unique index on such a hotel_id stored in the schema.

    See the Model and its parents defined in the api.core.database module
    for more information on the Mixins and Classes inherited by this class.
    """

    __tablename__ = "hotels"
    __id_prefix__ = "htl_"
    __table_args__ = (
        sa.Index('uq_idx_hotel_id', 'hotel_id', unique=True),
    )
    #
    # We use a field ID to depict the internal ID (primary key)
    # within our application and store the as 'hotel_id' the 'id'
    # from the connectivity demo endpoints.
    #
    hotel_id = sa.Column(sa.Integer, nullable=False)

    hotel_name = sa.Column(sa.String(100), nullable=False)
    num_reviews = sa.Column(sa.Integer, nullable=True)
    address = sa.Column(sa.Text, nullable=False)
    num_stars = sa.Column(sa.Integer, nullable=True)
    image_url = sa.Column(sa.Text, nullable=True)
    city = sa.Column(sa.String(100), nullable=False)

    #
    # We use postgres dialect implemented by SQLAlchemy to define an array
    # style column to store a hotel's amenities.
    #
    amenities = sa.Column(pg.ARRAY(sa.String), nullable=True)

    @property
    def prices(self):
        return HotelPrice.query.filter_by(hotel_id=self.hotel_id).all()
    #
    # A helper method to find prices for a hotel by date.
    #
    def price_by_date(self, check_in_date, check_out_date):
        return HotelPrice.query.filter_by(hotel_id=self.hotel_id,
                                          check_in_date=check_in_date,
                                          check_out_date=check_out_date)\
                               .order_by(HotelPrice.updated_at.desc())\
                               .first()
    #
    # A helper method to find prices for a city by date.
    #
    def price_by_city(self, city, check_in_date, check_out_date):
        return HotelPrice.query\
                        .filter(sa.func.lower(HotelPrice.city)==city.lower())\
                        .filter_by(
                            check_in_date=check_in_date,
                            check_out_date=check_out_date)\
                        .order_by(HotelPrice.check_in_date.asc())\
                        .all()

class HotelPrice(Model):
    """The hotel price models stores all the information regarding a hotels
    prices for a pair of check in and check out dates.

    NOTE: There exists a foreign key constraint on the hotel_id field which
    refers to the hotels.hotel_id field in the hotels schema.

    See the Model and its parents defined in the api.core.database module
    for more information on the Mixins and Classes inherited by this class.
    """
    __tablename__ = "hotel_prices"
    __id_prefix__ = "hp_"

    #
    # There was an assumption I made earlier where the triplet of hotel_id,
    # check_in_date and check_out_date would be true.
    # Tried to get in touch with the SnapTravel connectivity demo team but
    # could not.
    # To overcome this - have included an overwrite_prices argument in the
    # scripts and service to allow module objects to be overwritten
    # if required.
    #
    # Hence, the following __table_args__ was commented out.
    #
    #__table_args__ = (
    #    sa.Index('uq_idx_hotel_id_stay_dates', 'hotel_id', 'check_in_date',
    #             'check_out_date', unique=True),
    #)

    #
    # Define an explicit foreign key constraint which helps while navigating
    # related Hotel and HotelPrice objects and also shows up fine on the
    # database GUI tools.
    #
    __table_args__ = (
        db.ForeignKeyConstraint(
            ["hotel_id"],
            ["hotels.hotel_id"],
            name="fk_hotel_prices__hotel_id__hotels__hotel_id",
        ),
    )

    #
    # We use a field ID to depict the internal ID (primary key)
    # within our application and store the as 'hotel_id' the 'id'
    # from the connectivity demo endpoints.
    #
    hotel_id = sa.Column(sa.Integer, nullable=False)

    check_in_date = sa.Column(sa.Date, nullable=False)
    check_out_date = sa.Column(sa.Date, nullable=False)
    snap_travel_price = sa.Column(db.Numeric(10,4), nullable=False)
    retail_price = sa.Column(db.Numeric(10,4), nullable=False)

    #
    # SQLAlchemy's ORM relationship - a helpful one to refer 'parent' objects.
    #
    hotel = sa.orm.relationship(
        'Hotel',
        foreign_keys=[hotel_id],
        primaryjoin='Hotel.hotel_id == HotelPrice.hotel_id'
    )

    #
    # A human-readable representation of the Hotel Price object.
    #
    def __str__(self):
        return u'\nHotel Name: {}, \nHotel ID: {}, \nCity: {}, \nCheck-In: {},\
                 \nCheck-out: {}, \nSnapTravelPrice: {}, \nRetailPrice: {}\n'\
                 .format(self.hotel.hotel_name.title(), self.hotel_id,
                         self.hotel.city.title(),
                         self.check_in_date, self.check_out_date,
                         self.snap_travel_price, self.retail_price)
