# -*- coding: utf-8 -*-

"""We defined the fields and their translated key names (if required) to
enable easier re-use within the SnapTravel Connectivity Demo Flask Application.

"""

__all__ = [
    'REQUIRED_HOTEL_FIELDS',
    'REQUIRED_HOTEL_PRICE_FIELDS',
]

REQUIRED_HOTEL_FIELDS = {
    'id' : "hotel_id",
    'hotel_name' : "hotel_name",
    'address' : "address",
    'city' : "city"
}

REQUIRED_HOTEL_PRICE_FIELDS = {
    'id' : "hotel_id",
    'checkin' : "check_in_date",
    'checkout' : "check_out_date",
    'hotel_price'  : "snap_travel_price",
    'legacy_hotel_price' : "retail_price"
}
