# -*- coding: utf-8 -*-

"""A number of modular services registered with the ServiceRegistry which makes
them available throughout the SnapTravel Connectivity Demo Flask application.

"""

from flask import current_app

from sqlalchemy.orm.exc import NoResultFound

from api.core.database import db
from api.hotels.models import Hotel as HotelModel, \
    HotelPrice as HotelPriceModel

from .constants import *
from .utils import check_sanitize_parameters, update_object

class HotelUpdateService(object):
    """A collection of services for the Hotel model only.
    """
    def create_or_update_hotel_information(self, **params):
        """A method which updates or creates (if not present alredy)
        Hotel object and save in the persistence layer (PostgreSQL).

        :param:`params` -  A dictionary of key, value pairs which adhere to
            those required for creating/updating hotel price
            objects. See :constant:`REQUIRED_HOTEL_FIELDS` for more.

        :raises: ValueError if the required keys were not found in the
            :param:`params`. See the :constant:`REQUIRED_HOTEL_FIELDS`
            for more.

        :returns: - Hotel object which was created or updated.
        """
        #
        # Use the helper utility to check if all the required params have been
        # supplied by the caller.
        #
        args = check_sanitize_parameters(REQUIRED_HOTEL_FIELDS, **params)

        #
        # Use the current app's service framework to call hotel service's
        # find_hotel_by_id method with the ID (hotel_id) received as a reponse
        # from SnapTravel's APIs.
        #
        hotel = current_app.services.hotel_service\
                .find_hotel_by_id(args['id'])
        #
        # If an existsing hotel object was not found, instantiate a new one.
        #
        if not hotel:
            hotel = HotelModel()
        #
        # Use the helper utility to create or update the Hotel object and save
        # it in the persistence layer (PostgreSQL).
        #
        hotel = update_object(hotel, REQUIRED_HOTEL_FIELDS, args)
        db.session.commit()
        return hotel

class HotelPriceUpdateService(object):
    """A collection of services for the HotelPrice model only.
    """
    def find_hotel_prices(self, hotel_id, check_in_date=None,
                          check_out_date=None):
        """A helper method which fetches hotel price objects from the
        persistence layer (PostgreSQL).

        :param:`hotel_id` - The ID of the hotel recieved as a response from
            SnapTravel's APIs.
        :param:`check_in_date` - A datetime object which represents the date
            from which a reservation is requested or prices are to be fetched.
            NOTE: This parameter is optional.
        :param:`check_out_date` - A datetime object which represents the date
            upto which a reservation is requested or prices are to be fetched.
            NOTE: This parameter is optional.

        :returns: - HotelPrice objects which were found based on the passed
            param(s) else None if nothing was found matching the request
            param(s) or if any Hotel object itself matching :param:`hotel_id`
            was not in the persistence layer (PostgreSQL).
        """
        hotel = current_app.services.hotel_service\
                .find_hotel_by_id(hotel_id)
        if not hotel:
            return None
        elif not check_in_date or not check_out_date:
            return hotel.prices()
        else:
            return hotel.price_by_date(check_in_date, check_out_date)

    def create_or_update_hotel_price_information(self, hotel_id,
                                                 overwrite_prices=False,
                                                 **params):
        """A method which updates or creates (if not present alredy)
        HotelPrice object and save in the persistence layer (PostgreSQL).

        :param:`hotel_id` - The ID of the hotel recieved as a response from
            SnapTravel's APIs.
        :param:`overwrite_prices` - A boolean which drives whether hotel
            price objects are overwritten or not. If the triplet of
            information - hotel_id, check_in_date and check_out_date already
            has a matching object in the persistence layer (PostgreSQL).
            NOTE: This parameter is optional.
        :param:`params` -  A dictionary of key, value pairs which adhere to
            those required for creating/updating hotel price
            objects.

        :raises: ValueError if a matching Hotel object based on
            :param:`hotel_id` were not found in the persistence layer (PostgreSQL).
        :raises: ValueError if the required keys were not found in the
            :param:`params`. See the :constant:`REQUIRED_HOTEL_PRICE_FIELDS`
            for more.

        :returns: - Hotel Price object which was created or updated
            (depending on the :param:`overwrite_prices` value).
        """

        #
        # Use the helper utility to check if all the required params have been
        # supplied by the caller.
        #
        args = check_sanitize_parameters(REQUIRED_HOTEL_PRICE_FIELDS, **params)

        #
        # Just an extra check to see if the hotel exists based on the hotel_id
        # provided.
        #
        # Note: We're using the hotel_id and not the internal ID used in
        # the Hotel model.
        #
        hotel = current_app.services.hotel_service.find_hotel_by_id(hotel_id)
        if not hotel:
            raise ValueError("Hotel with ID: not found!".format(hotel_id))
        #
        # Initialize a new hotel price object which will be overwritten if
        # :param:`overwrite_prices` is True.
        #
        hotel_price = HotelPriceModel()
        if overwrite_prices:
            hotel_price = current_app.services\
                .hotel_price_update_service.find_hotel_prices(
                    hotel.hotel_id,
                    args['checkin'],
                    args['checkout'])
        #
        # Use the helper utility to create or update the HotelPrice object
        # and save it in the persistence layer (PostgreSQL).
        #
        hotel_price_model = update_object((hotel_price or HotelPriceModel()),
                                    REQUIRED_HOTEL_PRICE_FIELDS, args)
        db.session.commit()
        return hotel_price_model

class HotelService(object):
    """A collection of services for the Hotel and HotelPrice models.
    """
    def find_hotel_by_id(self, hotel_id):
        """A helper method to check if a hotel exists in the persistence layer
        based on the hotel id provided.

        :param:`hotel_id` - The ID of the hotel recieved as a response from
        SnapTravel's APIs.

        :returns: - Hotel object if found else None.
        """
        try:
            #
            # Note: We're using the hotel_id and not the internal ID used in
            # the Hotel model.
            #
            return HotelModel.query.filter_by(hotel_id=hotel_id).one()
        except NoResultFound:
            return None

    def create_or_update(self, overwrite_prices=False, **params):
        """A method which updates or creates (if not present alredy)
        Hotel & HotelPrice objects and save in the persistence layer (PostgreSQL).

        :param:`overwrite_prices` - A boolean which drives whether hotel
            price objects are overwritten or not (if the triplet of
            information - hotel_id, check_in_date and check_out_date) already
            has an object in the persistence layer (PostgreSQL).
            NOTE: This parameter is optional.
        :param:`params` -  A dictionary of key, value pairs which adhere to
            those required for creating/updating hotel and hotel price
            objects.

        :raises: ValueError if the required keys were not found in the
            :param:`params`. See the :constant:`REQUIRED_HOTEL_FIELDS`,
            :constant:`REQUIRED_HOTEL_PRICE_FIELDS` for more.

        :returns: - Hotel Price objects which were created or updated
            (depending on the :param:`overwrite_prices` value).
        """

        #
        # Use the helper utility to check if all the required params have been
        # supplied by the caller.
        #
        args = check_sanitize_parameters(REQUIRED_HOTEL_FIELDS,
                                         **params)
        #
        # Use the current app's service framework to call hotel service methods
        # create or update Hotel object based on the parsed values provided in
        # the args.
        #
        hotel = current_app.services\
                .hotel_update_service\
                .create_or_update_hotel_information(**args)

        #
        # Use the helper utility to check if all the required params have been
        # supplied by the caller.
        #
        args = check_sanitize_parameters(REQUIRED_HOTEL_PRICE_FIELDS,
                                         **params)

        #
        # Use the current app's service framework to call hotel service methods
        # create or update Hotel Price object based on the parsed values
        # provided in the args.
        #
        hotel_price = current_app.services\
                .hotel_price_update_service\
                .create_or_update_hotel_price_information(hotel.hotel_id,
                                                          overwrite_prices,
                                                          **args)
        return current_app.services\
                .hotel_price_update_service.find_hotel_prices(
                    hotel.hotel_id, args['checkin'], args['checkout'])
