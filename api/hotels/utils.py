# -*- coding: utf-8 -*-

"""A number of helper utilities used by the services defined under hotels
module. This promotes readablility, maintainability & re-usablility.

"""
from .constants import *

__all__ = [
    'check_sanitize_parameters',
    'update_object'
]

def check_sanitize_parameters(fieldset, **params):
    """A helper utility to check if all required keys are present on the
    params passed by the caller.

    :param:`fieldset` - This the dictionary against which the key, value pairs
        :param:`params` will be tested for presence and correctness.
    :param:`params` - A dictionary of key, value pairs send by the caller which
        need to be checked and sanitized.

    :raises: ValueError if the required keys were not found in the
        :param:`params`. See the :constant:`REQUIRED_`* defined under the
        constants file for more.

    :returns: - Same as params if all goes well and errors were found.
    """
    required_fields = set(fieldset.keys())
    parameters = set(params.keys())
    if not required_fields.issubset(parameters):
        raise ValueError("The following required fields are missing - {}"\
                     .format(required_fields-parameters))
    return params

def update_object(model_object, fieldset, args):
    """A helper utility to check if all required keys are present on the
    params passed by the caller.

    :param:`model_object` - The object which needs to be created or updated.
    :param:`fieldset` - This the dictionary to check against if the keys
        from :param:`args` need to be translated (if required).
    :param:`args` - A dictionary of key, value pairs which contains the
        attributes to be applied to the :param:`model_object`
        while creation/updation of the said object.

    :returns: - the object of the same type sent in by the caller (see
        :param:`model_object`) after saving in the current SQLAlchemy session.
        NOTE: The session needs to be committed by the caller for this object
        to be persisted in the database.
    """
    for attr in args.keys():
        internal_attr = fieldset[attr] \
                        if attr in fieldset.keys() \
                        else attr
        setattr(model_object, internal_attr, args[attr])
    model_object.save()
    return model_object
