# -*- coding: utf-8 -*-

"""A collection Hotel specific commands.

These commands interact with SnapTravel Connectivity Demo APIs and fetch
latest prices for hotels in cities based on certain check-in and check-out
dates.
"""

from flask_script import Manager

from .hotel_conectivity import HotelConnectivityCommand

__all__ = [
    'HotelCommand',
]

class HotelCommandManager(Manager):
    """Runs the SnapTravel Connectivity Demo via a CLI.

    We just need to register the class witht the flask script manager so we
    need an empty class.
    """
    pass

HotelCommand = HotelCommandManager(
    description=HotelCommandManager.__doc__
)

#
# Add the HotelConnectivityCommand to the flask script manager's repertoire.
#
HotelCommand.add_command('hotel_connectivity_demo',
                         HotelConnectivityCommand())
