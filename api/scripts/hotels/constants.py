# -*- coding: utf-8 -*-

"""We define the URLs and their associated key, value fields (if required) to
enable easier re-use within the hotel scripts modules - legacy or otherwise.
"""

__all__ = [
    'DEFAULT_PRICE_PROVIDER',
    'PRICE_FETCH_URLS',
    'DEFAULT_PRICE_FETCH_URL',
    'DEFAULT_REQUEST_BODY_MIME_TYPE',
    'DEFAULT_RESPONSE_BODY_MIME_TYPE',
    'ACCEPTABLE_BODY_MIME_TYPES'
]

DEFAULT_PRICE_PROVIDER = "snaptravel"
DEFAULT_PRICE_FETCH_URL = "hotels"

PRICE_FETCH_URLS = {
    'hotels' : {
        'url' : "https://experimentation.getsnaptravel.com/interview/hotels",
        'request_body_type' : "json",
        'response_body_type' : "json",
        'price' : "hotel_price",
    },
    'legacy_hotels' : {
        'url' : "https://experimentation.getsnaptravel.com/interview/legacy_hotels",
        'request_body_type' : "xml",
        'response_body_type' : "html",
        'price' : "legacy_hotel_price",
        'headers' : {'Content-Type': 'application/xml',
                     'Accept': 'application/xml'}
    },
}

DEFAULT_REQUEST_BODY_MIME_TYPE = PRICE_FETCH_URLS[DEFAULT_PRICE_FETCH_URL]\
                                    ['request_body_type']
DEFAULT_RESPONSE_BODY_MIME_TYPE = PRICE_FETCH_URLS[DEFAULT_PRICE_FETCH_URL]\
                                    ['response_body_type']

ACCEPTABLE_BODY_MIME_TYPES = ['json', 'xml', 'html']
