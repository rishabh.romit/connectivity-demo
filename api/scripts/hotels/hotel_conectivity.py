# -*- coding: utf-8 -*-

"""The command which takes care of hitting the SnapTravel Connectivity Demo
APIs, fetching the data and storing in our local persistance layer
(postgreSQL DB).

NOTE: As per the requirement of the SnapTravel Connectivity Demo we normalize
results (prices) received from the legacy_hotels and hotels API.
"""

import sys
import logging
import json
from datetime import datetime
import xml.etree.ElementTree as ET
from multiprocessing.pool import ThreadPool
from multiprocessing import Lock

import iso8601
import requests
import xmltodict

from flask_script import Option, Command
from flask import current_app

from .constants import *

logger = logging.getLogger(__name__)
message = 'HotelConnectivityCommand :: {}'

__all__ = [
    'HotelConnectivityCommand',
]

class HotelConnectivityCommand(Command):
    """A class which handles the SnapTravel Connectivity Demo being called
    via a CLI.

    NOTE: We an optional argument style options to pass in params to the while
    running this command.
    """

    #
    # A list of Option objects which lists command line arguments acceptable
    # by this CLI.
    #
    option_list = (
        Option('--city', '-c',
            dest='city',
            required=True,
            help='Checks hotel prices for a particular city.\
            Usage: -c=<city_name>\
            [Required: True]'),
        Option('--checkin', '-ci',
            dest='check_in_date',
            required=True,
            help='Checks hotel prices for a particular check out date.\
            Usage: -ci=<iso_8601_date>\
            [Required: True]'),
        Option('--checkout', '-co',
            dest='check_out_date',
            required=True,
            help='Checks hotel prices for a particular check out date.\
            Usage: -co=<iso_8601_date>\
            [Required: True]'),
        Option('--overwrite_prices', '-o',
            dest='overwrite_prices',
            required=False,
            default=False,
            help='Overwrites Prices if existing prices found for \
            check_in_date, check_out_date & same hotel_id\
            Usage: -o1\
            [Required: False]'),
    )

    def __init__(self):
        #
        # We store the response.content from the SnapTravel Connectivity Demo
        # API calls here.
        #
        self.results = {}

        #
        # Because, we're using a thread pool to make API calls we use a lock
        # while accessing the shared variable self.results by objects of this
        # class.
        #
        self.lock = Lock()

    def parse_date(self, date_string):
        """A helper to parse date strings passed in the command line.

        NOTE: We add validation to test for valid iso8601 style strings only.

        :param:`date_string` - An iso8601 style acceptable date.

        :raises:`iso8601.iso8601.ParseError` - If the :param:`date_string` does
            not qualify as a valid is08601 date.
            NOTE: The script will exit after catching and logging the error.

        :returns: - A datetime.date object after parsing the
            :param:`date_string`.
        """
        try:
            return iso8601.parse_date(date_string).date()
        except iso8601.iso8601.ParseError:
            logger.warn(message.format('{} is not a valid date')\
                            .format(date_string))
            sys.exit(-1)

    def prepare_request_body(self, body_type=DEFAULT_REQUEST_BODY_MIME_TYPE,
                             **params):
        """A helper utiltiy which prepares request body based on the various
        types of API endpoints and request body and mime_types by them.

        param:`body_type` - The MIME type of the request body which drives the
            request body preparation.
            NOTE: This is an optional field.
        param:`params` - A dictionary of key, value pairs which holds the
            arguments required by theSnapTravel Connectivity Demo APIs (legacy
            or otherwise).

        :raises:`ValueError` - If the :param:`body_type` is not present in a
            list of acceptable body types.

        :returns: - The request body based on the :param:`body_type` passed in
            as an argument.
        """
        if body_type in ACCEPTABLE_BODY_MIME_TYPES:
            if body_type == 'json':
                return {
                'city' : str(params['city']),
                'checkin' : str(params['check_in']),
                'checkout' : str(params['check_out']),
                'provider' : DEFAULT_PRICE_PROVIDER,
            }
            elif body_type == 'xml' :
                root = ET.Element("root")
                ET.SubElement(root, "city").text = params['city']
                ET.SubElement(root, "checkin").text = str(params['check_in'])
                ET.SubElement(root, "checkout").text = str(params['check_out'])
                ET.SubElement(root, "provider").text = DEFAULT_PRICE_PROVIDER
                return ET.tostring(root, encoding='utf8', method='xml')

        else:
            raise ValueError(
                'body_type: {0} is not in {1}'\
                    .format(body_type,
                            ACCEPTABLE_REQUEST_BODY_MIME_TYPES)
            )

    def parse_response_body(self, response_content,
                            body_type=DEFAULT_RESPONSE_BODY_MIME_TYPE):
        """A helper utiltiy which prepares response contenton the various
        types of API endpoints and reponse body and mime_types by them.

        param:`body_type` - The MIME type of the response body which drives the
            response body preparation.
            NOTE: This is an optional field.
        param:`response_content` - The string response content received after
            hitting the SnapTravel Connectivity Demo API.

        :raises:`ValueError` - If the :param:`body_type` is not present in a
            list of acceptable body types.

        :returns: - The request body based on the :param:`body_type` passed in
            as an argument.
        """
        if body_type in ACCEPTABLE_BODY_MIME_TYPES:
            if body_type == 'json':
                return {
                    'response' : json.loads(response_content),
                    'type' : body_type
                }
            elif body_type == 'html':
                #
                # The response recieved is a string which is an XML but the
                # MIME type set in the response was HTML.
                #
                return {
                    'response' : response_content,
                    'type' : 'xml'
                }
        else:
            raise ValueError(
                'body_type: {0} is not in {1}'\
                    .format(body_type,
                            ACCEPTABLE_BODY_MIME_TYPES)
            )

    def normalize_results(self, results):
        """A method which implements the normalization requirement of the
        SnapTravel Connectivity Demo.

        NOTE: It was a requirement to only store results if hotel id's were
        found in both the API responses (legacy or otherwise)

        param:`results` - The results that were parsed after hitting the APIs.

        :returns: - normalized results based on the follwing fact - if
            a hotel ID was found in both responses (legacy API or hotels API)
            we use the 'price' from legacy API as 'retail_price' and 'price'
            from hotels API as 'snap_travel_price'.
        """
        normalized_results = []
        legacy_hotels = []

        #
        # Parse the XML received from legacy API as a JSON - makes it easier
        # to be handeld within this script.
        #
        legacy = json.loads(
            json.dumps(
                xmltodict.parse(results['legacy_hotels']['response'])
            )
        )

        #
        # Prepare a dictionary of hotel IDs and prices recieved from the legacy
        # API.
        #
        for lh in legacy['root']['element']:
            legacy_hotels.append({
                'id' : int(lh['id']),
                'price' : lh['price'],
            })

        #
        # Iterate over the 'new' hotels API results.
        #
        for hotel in results['hotels']['response']['hotels']:
            #
            # Check if the ID of the current hotel being iterated upon was
            # also present in the legacy API results.
            #
            legacy_index = next((index for (index, d) \
                                 in enumerate(legacy_hotels) \
                                 if d['id'] == hotel['id']),
                                None)
            #
            # If found, store all information from 'hotels' API of the
            # SnapTravel Connectivity Demo, but also set the 'price' from
            # legacy API as 'retail_price' and 'price' from hotels API as
            # 'snap_travel_price'.
            #
            # See the :constant:`PRICE_FETCH_URLS` for more information on how
            # the key, vaue pairs are setup.
            #
            if legacy_index != None:
                _result = {}
                for k, v in hotel.iteritems():
                    if k in PRICE_FETCH_URLS['hotels']:
                        _result.update({PRICE_FETCH_URLS['hotels'][k] : v})
                    else:
                        _result.update({k : v})
                    _result.update({
                        PRICE_FETCH_URLS['legacy_hotels']['price'] : \
                            legacy_hotels[legacy_index]['price']
                    })
                normalized_results.append(_result)
        return normalized_results

    def threaded_requests(self, request=DEFAULT_PRICE_FETCH_URL):
        """The function which each thread runs when given a chance.

        This function is where we hit the SnapTravel Connectivity Demo APIs.

        :param:`request` - The key to the :constant:`PRICE_FETCH_URLS` which
            determines which URLs, body and header will be used when hitting
            the SnapTravel Connectivity Demo APIs using the HTTP post method.

        :returns: - The HTTP status code and response content if the API
            was hit successfully and response was parsed.
            Returns an Error and empty response in case of an Exception.
        """
        try:
            response = requests.post(PRICE_FETCH_URLS[request]['url'],
                               data=self.prepare_request_body(
                                   PRICE_FETCH_URLS[request]\
                                   ['request_body_type'], **self.params),
                               headers=PRICE_FETCH_URLS[request]['headers'] \
                                if 'headers' in PRICE_FETCH_URLS[request] \
                                else None
                               )
            if response.status_code == 200:
                #
                # Acquire a lock before accessing the shared self.results
                # variable.
                #
                self.lock.acquire()
                self.results[request] = self.parse_response_body(
                    response.content,
                    response.headers['content-type']\
                        .split(';')[0]\
                        .split('/')[1]
                )
                #
                # Release the lock before continuing to allow other threads
                # within the same class object to access the shared variable.
                #
                self.lock.release()
                return response.status_code, response
        except Exception as e:
            return e, None

    def run(self, city, check_in_date, check_out_date,
            overwrite_prices=False):
        """The entry point of the SnapTravel Connectivity Demo hotels CLI.

        :param:`city` - A string with the city name for which hotel prices are
            to be fetched.
        :param:`check_in_date` - An iso8601 style date string.
        :param:`check_out_date` - An iso8601 style date string.
        :param:`overwrite_prices` - This boolean flag drives whether prices
            are overwritten in the persistance layer (PostgreSQL) if matching
            object already exists (matching the triplet of fields - hotel id,
            check_in_date, check_out_date).
            NOTE: This argument is optional.

        :returns: - None but prints human-readable hotel and price information
            after reading from the persistance layer (PostgreSQL).
        """

        #
        # Use the helper to see if the date strings passed in as command line
        # arguments are valid is08601 date strings.
        #
        check_in = self.parse_date(check_in_date)
        check_out = self.parse_date(check_out_date)

        #
        # Just swap the check_in, check_out dates if (by mistake) the
        # check_out_date happens to be after check_in_date. Just a
        # convinience instead of raising an explicit error.
        #
        if check_out < check_in:
            check_in, check_out = check_out, check_in

        logger.info(message.format('Fetching Prices for {} from: {} to: {}'\
                                   .format(city,
                                           check_in,
                                           check_out)))
        #
        # Prepare the parameters which will form the request body when hittting
        # the SnapTravel Connectivity Demo APIs.
        #
        self.params = {
            'city' : city,
            'check_in' : check_in,
            'check_out' : check_out,
        }

        #
        # Start the threads and let them hit the SnapTravel Connectivity Demo
        # APIs.
        #
        results = ThreadPool(2).imap_unordered(self.threaded_requests,
                                               PRICE_FETCH_URLS.keys())
        #
        # Wait for the results and proceed only if all of them were 200-OK.
        #
        for status, response_content in results:
            if status != 200:
                logger.error(message.format(status))
                exit(-1)
        #
        # If both the APIs were successfully hit & their responses parsed
        # we proceed by 'normalizing' the responses as described above.
        #
        if len(self.results.keys()) == 2:
            logger.info(message\
                        .format('Normalizing hotel & price information'))
            normalized_results = self.normalize_results(self.results)
            for n in normalized_results:
                n.update({
                    'checkin' : check_in,
                    'checkout' : check_out,
                    'city' : city,
                })

                #
                # Use the current flask app's Service Registry to look for and
                # call the service which creates or updates Hotel and
                # HotelPrice objects and makes them persistant in the
                # database (postgreSQL).
                #
                hotels = current_app.services.hotel_service\
                        .create_or_update(overwrite_prices, **n)
                logger.info(message\
                    .format(
                        'Hotel and Price Information created/updated as -')
                    )

                #
                # Print human-readable hotel and price information after
                # reading from the persistance layer (PostgreSQL)
                #
                if isinstance(hotels, (list, tuple)):
                    for h in hotels:
                        print h
                else:
                    print hotels
