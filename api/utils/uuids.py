# -*- coding: utf-8 -*-

"""Helper utilies which deal with UUID generation.

"""

import shortuuid

__all__ = [
    'uuid_with_prefix'
]

def uuid_with_prefix(prefix=None):
    """A helper utility to allow a prefix to be attached when generating
    UUIDs.
    """
    return shortuuid.uuid() if not prefix \
        else prefix + shortuuid.uuid()
