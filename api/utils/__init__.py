# -*- coding: utf-8 -*-

"""A list of utilities defined under this module which are used throughout the
Connectivity Demo Flask Application.
"""

from .uuids import uuid_with_prefix
