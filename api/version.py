# -*- coding: utf-8 -*-

"""This file defines where we store the version of SnapTravel Connectivity Demo
Flask Application.

"""
__version__ = "1.0.0"
