# -*- coding: utf-8 -*-

"""A module which allows the SnapTravel Connectivity Demo Flask Application to
be as a local development server.

We create new a Flask application when using the 'Procfile' method of
launching the application. See Procfile for more.
"""

import logging

from .create_flask_application import create_app
from .version import __version__ as version

logger = logging.getLogger(__name__)

#
# Create the SnapTravel Connectivity Demo Flask Application instance.
#
app = create_app()
