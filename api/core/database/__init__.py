# -*- coding: utf-8 -*-

"""The core of all the database related 'magic' happens and is available
throughout the SnapTravel Connectivity Demo Flask Application.
"""

from datetime import datetime

import sqlalchemy as sa
from flask_sqlalchemy import SQLAlchemy

from api.utils import uuids

#
# The db is the global sqlalchmey object which is used to perform queries,
# updates, inserts, deletes etc... throughout the application.
#

db = SQLAlchemy(session_options={'autoflush':False})
db.Model.metadata.naming_convention = {
    'ix': 'ix_%(column_0_label)s',
    'uq': 'uq_%(table_name)s_%(column_0_name)s',
    'ck': 'ck_%(table_name)s_%(constraint_name)s',
    'fk': 'fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s',
    'pk': 'pk_%(table_name)s',
}

__all__ = [
    'db',
    'Model',
]

class BaseModelMixin(object):
    """A mixin to aid models development within the Flask application.

    This has all the common 'elements' of a model which may be used to create
    schemas by the modules when required - namely: id, created_at, updated_at

    NOTE: This allows us to create modular and re-usable models without having
    to repeatedly define the parts of schemas which are common to all.
    """
    created_at = sa.Column(sa.DateTime(timezone=True),
                           default=datetime.utcnow())
    updated_at = sa.Column(sa.DateTime(timezone=True),
                           default=datetime.utcnow(),
                           onupdate=datetime.utcnow())

    @sa.ext.declarative.declared_attr
    def id(cls):
        prefix = getattr(cls, '__id_prefix__', None)
        return sa.Column('id', sa.String, primary_key=True,
                         default=lambda: uuids.uuid_with_prefix(prefix))

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id)

    def save(self):
        db.session.add(self)
        db.session.flush()

    def __repr__(self):
        return '{}({!r})'.format(self.__class__.__name__, self.id)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return u'{}({})'.format(self.__class__.__name__, self.id)

class Model(db.Model, BaseModelMixin):
    """An abstract class which will be used by modules within the application
    when bulding models for persistence in a database.

    NOTE: This inherits from the BaseModelMixin we have defined above.
    """
    __abstract__ = True
