# -*- coding: utf-8 -*-

"""A list of CORE servcies which are used by the modules defined throughout
the Connectivity Demo Flask Application.

This allows us to build re-usable modular core services which may be used from
within modules of this application. For now, we have only two such core
services but setting it up like this allows us to scale the application when
we add more core services for e.g. like caching, events, sessions etc...
"""

from .database import db
from .services import ServiceRegistry

EXTENSIONS = [
    db,
]

__all__ = [
    'db',
    'ServiceRegistry'
]
