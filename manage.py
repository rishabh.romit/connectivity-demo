#!/usr/bin/env python

#
# This kickstarts the Flask Application for the SnapTravel connectivity demo.
#
if __name__ == '__main__':
    from api import run_manage_py
    run_manage_py()
