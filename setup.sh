#!/bin/bash
export POSTGRES_PASSWORD=T45YrKbg
export POSTGRES_USER=postgres
export POSTGRES_DB=connectivity_demo
mkdir -p $HOME/.virtualenvs
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python

echo "Installing Pip..."
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
hash -r

echo "Pulling Latest PostgreSQL (Docker) Image..."
if ! docker pull postgres; then
    echo "  This setup assumed Docker is installed on this machine.
  If not, see: https://docs.docker.com/v17.09/engine/installation/
"
    exit -1
fi

echo "Installing VirtualEnv..."
pip install virtualenvwrapper
mkdir -p $HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

echo "Creating & switching to VirtualEnv for SnapTravel Connectivity Demo..."
mkvirtualenv connectivity-demo
workon connectivity-demo

echo "Running PostgreSQL (Docker)..."
docker run --rm --name connectivity-demo -e POSTGRES_USER=$POSTGRES_USER -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -e POSTGRES_DB=$POSTGRES_DB -d -p 54399:5432 postgres

echo "Installing Requirements..."
pip install -r requirements.txt
env LDFLAGS="`pg_config --ldflags`" pip install psycopg2==2.8.3 --no-binary psycopg2

echo "Applying database migrations..."
./scripts/run alembic upgrade head

echo
echo "Initial Setup Complete - See Usage below"
echo
./scripts/run python manage.py hotels hotel_connectivity_demo --help
